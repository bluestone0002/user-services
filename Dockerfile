FROM node:10
WORKDIR /usr/src/userservice
COPY node_modules .
RUN npm install
COPY . .
EXPOSE 2001
CMD ["node", "users.js"]