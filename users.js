const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const User = require('./User');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 2001;

app.use(cors());
app.use(express.json());

const uri = "mongodb://mongo:27017/todousers";
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

app.get('/', (req, res) => {
  User.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
});

app.post('/add', (req, res) => {
  const username = req.body.username;
  const newUser = new User({username});
  newUser.save()
    .then(() => res.json('User added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});